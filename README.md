# Blur Detection Application

## Requirements

This application was build using the following versions. It may work with older versions, it may not. 
 - Python 3.8 
 - Node.js v16.15.1

If you want to make use of your GPU you need:

 - CUDA
 - CUDnn

## How to develop

If you want to develop this app first install the needed python and node dependencies (see "How to build"). 
Open two command windows and start the frontend and backend application in two seperate windows.

Starting the electron app:

    npm run start:dev

Starting the flask app:

    python ./backend/flask_backend.py

 


## How to build 

 1. Build the backend flask application 
	 * Switch to the backend folder
		 * `cd backend`
	 * Install python dependencies
		 * `pip install -r requirements.txt`
	 * Create dist
		 * `pyinstaller flask_backend.py`
	 * Copy model files into the backend dist
		  * `copy .\model_blur.h5 .\dist\flask_backend\`
		*  `copy .\model_light.h5 .\dist\flask_backend\`
		  * `copy .\model_dark.h5 .\dist\flask_backend\`
	  * Switch back to the main folder
		  * `cd ..`
 2. Build the React application
	 * Install js dependencies
		 * `npm install`
	 *  Run the build script
		 * `npm run build`
	 * Package the application
		 * `npm run package`

   



