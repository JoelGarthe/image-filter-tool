import '../App.css';
import { Dialog } from 'primereact/dialog';
import CustomEventBus from '../CustomEventBus';
import React, { useState } from 'react';
import { ProgressBar } from 'primereact/progressbar';


/**
 * 
 * Modal that displays the current loading status of the backend component. 
 * 
 */



// Return a message if the loading process has not started yet
function checkLoadingStatus(val){
    console.log(val)
    return val == 0 ? "Initializing neural networks (may take a few seconds)...": "Loading" 
}

function LoadingModal() {
    const [displayBasic, setDisplayBasic] = useState(false);
    const [progress, setProgress] = useState(10);

    // Remove all prev. event handler to avoid duplicates.
    CustomEventBus.removeAllListeners("OpenLoadModal")
    CustomEventBus.removeAllListeners("CloseLoadModal")
    CustomEventBus.removeAllListeners("SetLoadingProgess")
    
    // Reset the progress when the modal is opened. 
    CustomEventBus.on("OpenLoadFileModal", () => {
        setDisplayBasic(true)
        setProgress(0)
    });

    // Reset the progress when the modal is closed
    CustomEventBus.on("CloseLoadFileModal", () => {
        setDisplayBasic(false)
        setProgress(0)
    });

    // Set the updated loading progress
    CustomEventBus.on("SetLoadingProgess", (loadingProgress) => {
        setProgress(loadingProgress)
    });


    const onHide = () => {
        setDisplayBasic(false);
    }

    return (
        <div>
            <Dialog header={checkLoadingStatus(progress)} visible={displayBasic} style={{ width: '50vw' }} onHide={() => onHide()} closable={false} draggable={false}>
                
                <ProgressBar value={Math.round(progress)} />
            </Dialog>
        </div>
    );
}

export default LoadingModal;
