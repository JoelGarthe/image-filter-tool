import React, { useState, useEffect, useRef } from 'react';
import { Button } from 'primereact/button';
import { Galleria } from 'primereact/galleria';
import { classNames } from 'primereact/utils';
import './GalleriaAdvancedDemo.css';
import useKeypress from 'react-use-keypress';
import { Checkbox } from 'primereact/checkbox';
import CustomEventBus from '../CustomEventBus';

export default function Gallery(props) {
    const [images, setImages] = useState(null);
    const [activeIndex, setActiveIndex] = useState(0);
    const [isAutoPlayActive, setAutoPlayActive] = useState(true);
    const [isFullScreen, setFullScreen] = useState(false);
    
    const galleria = useRef(null)

    const responsiveOptions = [
        {
            breakpoint: '1024px',
            numVisible: 5
        },
        {
            breakpoint: '960px',
            numVisible: 4
        },
        {
            breakpoint: '768px',
            numVisible: 3
        },
        {
            breakpoint: '560px',
            numVisible: 1
        }
    ];

    useEffect(() => {
        //PhotoService.getImages().then(data => setImages(data));

        let newImages = props.rows.map((row, rowIdx) => {

            if(row.name == props.activeRow.name){
                setActiveIndex(rowIdx)
            }

            row.itemImageSrc = `file:///${row.name}`

            return row
        })

        setImages(newImages)

        bindDocumentListeners();

        return () => unbindDocumentListeners();
    },[]);

    useEffect(() => {
        setAutoPlayActive(galleria.current.isAutoPlayActive())
    },[isAutoPlayActive]);

    const onItemChange = (event) => {
        setActiveIndex(event.index)
    }

    const toggleFullScreen = () => {
        if (isFullScreen) {
            closeFullScreen();
        }
        else {
            openFullScreen();
        }
    }

    const onFullScreenChange = () => {
        setFullScreen(prevState => !prevState )
    }

    const openFullScreen = () => {
        let elem = document.querySelector('.custom-galleria');
        if (elem.requestFullscreen) {
            elem.requestFullscreen();
        }
        else if (elem.mozRequestFullScreen) { /* Firefox */
            elem.mozRequestFullScreen();
        }
        else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
            elem.webkitRequestFullscreen();
        }
        else if (elem.msRequestFullscreen) { /* IE/Edge */
            elem.msRequestFullscreen();
        }
    }

    const closeFullScreen = () => {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }
        else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        }
        else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }
        else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
    }

    
    useKeypress(['ArrowLeft', 'ArrowRight','ArrowUp', 'ArrowDown'], (event) => {

        switch(event.key){
            case "ArrowLeft":
                if(activeIndex == 0)
                    setActiveIndex(images.length-1)
                else
                    setActiveIndex(activeIndex -1)
                break

            case "ArrowRight":
                setActiveIndex((activeIndex+1) % images.length)
                break

            case "ArrowUp":
                setCurrentCheck(true)
                break

            case "ArrowDown":
                setCurrentCheck(false)
                break
        }
    });

    function setCurrentCheck(value){

        // Array needs to be remapped and set, otherwise the ui does not update
        // Look into useState in combination with arrays for more information
        const nextImageState = images.map(image => {
            if(image === images[activeIndex]){
                image.selected = value
                return image   
            }
            return image
        })

        setImages(nextImageState)

    }

    function switchCurrentCheck(){
        setCurrentCheck(!images[activeIndex].selected)
    }

    const bindDocumentListeners = () => {
        document.addEventListener("fullscreenchange", onFullScreenChange);
        document.addEventListener("mozfullscreenchange", onFullScreenChange);
        document.addEventListener("webkitfullscreenchange", onFullScreenChange);
        document.addEventListener("msfullscreenchange", onFullScreenChange);
    }

    const unbindDocumentListeners = () => {
        document.removeEventListener("fullscreenchange", onFullScreenChange);
        document.removeEventListener("mozfullscreenchange", onFullScreenChange);
        document.removeEventListener("webkitfullscreenchange", onFullScreenChange);
        document.removeEventListener("msfullscreenchange", onFullScreenChange);
    }

    const itemTemplate = (item) => {

        let fullScreenClassName = classNames('pi', {
            'pi-window-maximize': !isFullScreen,
            'pi-window-minimize': isFullScreen
        });



        if (isFullScreen) {
            return (
                <>
                <img src={item.itemImageSrc} alt={item.alt}  style={{ height: "100vh" }}/>
                
                <div className="imageInfo">
                    <Checkbox onChange={(e) => switchCurrentCheck()} checked={images[activeIndex].selected} ></Checkbox>
                    <span className='title-container'>
                        <span>{activeIndex + 1}/{images.length}</span>
                        <span className="title">{images[activeIndex].title}</span>
                          
                    </span>
                    <Button icon={fullScreenClassName} onClick={() => toggleFullScreen()} className="fullscreen-button" /> 
                </div>
                </>
            )

        }

        return (
                <>
                <img src={item.itemImageSrc} alt={item.alt} style={{ width: '100%', display: 'block' }} />

                <div className="imageInfo">
                <Checkbox onChange={(e) => switchCurrentCheck()} checked={images[activeIndex].selected}></Checkbox>
                    <span className='title-container'>
                        <span>{activeIndex + 1}/{images.length}</span>
                        <span className="title">{images[activeIndex].display_name}</span>
                          
                    </span>
                    <Button icon={fullScreenClassName} onClick={() => toggleFullScreen()} className="fullscreen-button" /> 
                </div>

                </>

        )
        
    }



    const galleriaClassName = classNames('custom-galleria', {
        'fullscreen': isFullScreen
    });

    return (
        <div className="card galleria-demo">
            <Galleria ref={galleria} value={images} activeIndex={activeIndex} onItemChange={onItemChange}
                showThumbnails={false} showItemNavigators showItemNavigatorsOnHover
                numVisible={3} circular responsiveOptions={responsiveOptions}
                item={itemTemplate} 
                style={{ maxWidth: '750px', margin: "auto" }} className={galleriaClassName} />
        </div>
    )
}
        