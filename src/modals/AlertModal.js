import '../App.css';
import { Dialog } from 'primereact/dialog';
import CustomEventBus from '../CustomEventBus';
import React, { useState } from 'react';
import { Button } from 'primereact/button';
import { Chip } from 'primereact/chip';
const { ipcRenderer } = window;

/**
 * 
 * Alert modal that opens when you want to move files and 
 * asks for confirmation. Allows you to change the path the
 * files are moved to. 
 * 
 */



function AlertModal() {
    const [displayBasic, setDisplayBasic] = useState(false);
    const [folderPath, setFolderPath] = useState("");
    
    // Remove all prev. event handler to avoid duplicates.
    CustomEventBus.removeAllListeners("OpenMoveFileModal")
    ipcRenderer.removeAllListeners("SelectedMovePath")
    
    // Open the modal
    CustomEventBus.on("OpenMoveFileModal", (folderpath) => {
        setFolderPath(folderpath)
        setDisplayBasic(true)
    });

    // If another path to move the files to was selected set it
    ipcRenderer.on('SelectedMovePath', (event, folderpath) => {
        setFolderPath(folderpath)
    })

    // If you confirm the modal move the files
    const onConfirm = (position) => {
        CustomEventBus.send("MoveSelectedFiles", folderPath)
        setDisplayBasic(false);
    }

    // If you close the modal
    const onDeny = (position) => {
        setDisplayBasic(false);
    }

    const renderFooter = (name) => {
        return (
            <div>
                <Button label="Yes" icon="pi pi-check" onClick={() => onConfirm()} autoFocus />
                <Button label="No" icon="pi pi-times" onClick={() => onDeny()} className="p-button-text" />
            </div>
        );
    }


    const onHide = (name) => {
        setDisplayBasic(false);
    }

    return (
        <div>
            <Dialog header="Moving Files" visible={displayBasic} style={{ width: '50vw' }} footer={renderFooter()} onHide={() => onHide()}>
                <p>Do you want to move the selected file(s) into the folder specified below?</p>

                <div className='pathMenu'>
                    <Chip label={folderPath} /><Button  icon="pi pi-folder" style={{ height: '2em' }}  onClick={() => ipcRenderer.send("SelectMovePath")} />
                </div>

                
            </Dialog>
        </div>
    );
}

export default AlertModal;
