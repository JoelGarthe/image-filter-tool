import '../App.css';
import { Dialog } from 'primereact/dialog';
import CustomEventBus from '../CustomEventBus';
import React, { useState } from 'react';
import Gallery from './Gallery'
/**
 * 
 * Model that displays an image that was clicked on in the data table
 * 
 * TODO: Add function to quickly switch through images and select/deselect them.
 * 
 */


function ImageModal() {
    const [displayModal, setDisplayModal] = useState(false);
    const [rows, setRows] = useState(null);
    const [activeRow, setActiveRow] = useState(null);


    // Remove all prev. event handler to avoid duplicates.
    CustomEventBus.removeAllListeners("OpenImageModal")
    CustomEventBus.removeAllListeners("CloseImageModal")
    
    // If the modal was opened load the image and display
    CustomEventBus.on("OpenImageModal", (data) => {
        //setImagePath(imagePath)

        setRows(data.rows) 
        setActiveRow(data.activeRow)
        setDisplayModal(true)
    });


    CustomEventBus.on("CloseImageModal", () => {
        setDisplayModal(false)
    });

    const onHide = () => {
        CustomEventBus.send("ReturnFromGallery", rows)
        setDisplayModal(false);
    }

    return (
        <>
        <div >
            <Dialog visible={displayModal} /*style={{ width: '95%'}} */onHide={() => onHide()} closable='true' className='imageGallery' >
                <Gallery rows={rows} activeRow={activeRow} ></Gallery>
            </Dialog>
        </div>
        </>
        
    );
}

export default ImageModal;
