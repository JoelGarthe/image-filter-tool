var listenerStack = []

/**
 * Create a custom event bus that is used to pass information between different react components,
 * withoud needing to traverse the component hierarchy 
*/


function on(eventName, callback){
    listenerStack.push({eventName, callback})
}

function send(eventName, payload){
    listenerStack.forEach(element => {
        if(element.eventName == eventName)
            element.callback(payload)
    });
}

// Remove all registered events with the specified name
function removeAllListeners(eventName){
    listenerStack = listenerStack.filter(item => item.eventName !== eventName)
}

// Export event bus functions
export default  {
    on, send, removeAllListeners
}
