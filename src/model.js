
import axios from 'axios';
import CustomEventBus from './CustomEventBus';

/**
 * Script that implements calls to the python backend.
 */


/**
 * Retrieve the loading state of the backend every second until the processing is finished. 
 */
async function loadingLoop(){

    let intervalID = setInterval(async () => {
        const res = await axios.get('http://127.0.0.1:5029/loadStatus', {}).catch(function (error) {
            if (error.response) {
              // The request was made and the server responded with a status code
              // that falls out of the range of 2xx
              console.log(error.response.data);
              console.log(error.response.status);
              console.log(error.response.headers);

              clearInterval(intervalID);
            }
            console.log(error.config);
            return []
        });

        if (res.data[0] == -1) {
            clearInterval(intervalID); // Stop the interval if the condition holds true
        }

        CustomEventBus.send("SetLoadingProgess", res.data[0])

    }, 1000);

}


/**
 * Send a predition request to the backend and retrieve the results
 */
async function predictRows(rows, quality) {

    let fileNames = []

    rows.forEach(row => {
        fileNames.push(row.name)
    })

    let response_promise = axios.post('http://127.0.0.1:5029/predict', {
        "files": fileNames,
        "quality":quality
    })

    // Start retrieving the loading state
    loadingLoop()

    let response = await response_promise
    response.data.forEach(resdata => {
        rows.forEach(row => {
            if (row.name == resdata.file){
                row.blur_value = parseFloat(resdata.blur * 100).toFixed(2)
                row.light_value = parseFloat(resdata.light * 100).toFixed(2)
                row.dark_value = parseFloat(resdata.dark * 100).toFixed(2)
            }
        })

    });

    return rows
}





export {
    predictRows
}