
import React, { useState, useEffect } from 'react';
import { Dropdown } from 'primereact/dropdown';
import { Button } from 'primereact/button';
import Filter from './Filter';
import CustomEventBus from './CustomEventBus';
const { ipcRenderer } = window;


/**
 * 
 * React component that implements the options on the sidebar
 * 
 */


function handleClickLoad(){
    ipcRenderer.send("LoadFiles")
}

const Sidebar = () => {
    const quality = [
        //{ name: 'Rough' },
        { name: 'Medium'},
        { name: 'Fine' },
    ];

    function handleClickPredict(){
        CustomEventBus.send("PredictImages", selectedQuality.name)
    
    }

    const [selectedQuality, setSelectedQuality] = useState(quality[0]);


    return (
        <div>
            <h3>Data</h3>
            <div className='sideBarContent'>
                <div className='buttonContainer'>
                    <Button label="Import files" onClick={handleClickLoad}/>  
                </div>
            </div>
            <h3>Processing</h3>
            <div className='sideBarContent'>
                <div className='buttonContainer'>
                    <Button label="Process files" onClick={handleClickPredict}/>
                </div>
                <h3>Blur detection quality</h3>
                <div className='buttonContainer'>
                    <Dropdown value={selectedQuality} onChange={(e) => setSelectedQuality(e.value)} options={quality} optionLabel="name" className="w-full" />
                </div>
            </div>
            <h3>Filter by...</h3>
            <div className='sideBarContent'>
                <Filter/>
            </div>
        </div>
    );
}
         
export default Sidebar