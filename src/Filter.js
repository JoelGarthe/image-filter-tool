import React, { useState } from 'react';
import { Slider } from 'primereact/slider';
import { Checkbox } from 'primereact/checkbox';
import { Button } from 'primereact/button';
import CustomEventBus from './CustomEventBus';
import './App.css';

/**
 * 
 * React component that implements the filter menu and sliders. Allows you to select and 
 * deselect filters. Updates the datatable accordingly if the filter button is pressed. 
 * 
 */

const Filter = () => {
    const min_filter_range = 0
    const max_filter_range = 100

    const [blurFilterValue, setBlurFilterValue] = useState(10);
    const [blurFilterCheck, setBlurFilterCheck] = useState(false);
    const [lightFilterValue, setLightFilterValue] = useState(10);
    const [lightFilterCheck, setLightFilterCheck] = useState(false);
    const [darkFilterValue, setDarkFilterValue] = useState(10);
    const [darkFilterCheck, setDarkFilterCheck] = useState(false);


    function handleClickFilterFiles(){
        CustomEventBus.send("FilterFiles", {
            "blur": blurFilterCheck ? blurFilterValue : -1,
            "light": lightFilterCheck ? lightFilterValue : -1,
            "dark": darkFilterCheck ? darkFilterValue : -1
        })
    }
    
    function handleClickMoveFiles(){
        CustomEventBus.send("OpenMoveFileModalGetPath")
    }

    function switchBlurFilterState(){
        setBlurFilterCheck(!blurFilterCheck)
    }
    function switchDarkFilterState(){
        setDarkFilterCheck(!darkFilterCheck)
    }
    function switchLightFilterState(){
        setLightFilterCheck(!lightFilterCheck)
    }

    function getFilterDropdownIcon(currState){
        if(currState)
            return "filterIcon pi pi-angle-down"
        else
            return "filterIcon pi pi-angle-right"
    }
    // <Checkbox onChange={e => setBlurFilterCheck(e.checked)} checked={blurFilterCheck} icon=" pi pi-angle-down"></Checkbox>

    return (
        <div>
            <div className='filterContainer'>
                <div className='filterCheckbox' onClick={switchBlurFilterState}>
                    <i className={getFilterDropdownIcon(blurFilterCheck)} style={{fontSize: "1.5em"}}></i><b>{blurFilterCheck ?`Blur > ${blurFilterValue} % `: 'Blur'}</b>
                </div>
                
                <div className='filterSlider' style={{display:blurFilterCheck ? 'block' : 'none' }}>
                    <Slider value={blurFilterValue} onChange={(e) => setBlurFilterValue(e.value)} min={min_filter_range} max={max_filter_range} />
                </div>
            </div>


            <div className='filterContainer'>
                <div className='filterCheckbox' onClick={switchLightFilterState}>
                    <i className={getFilterDropdownIcon(lightFilterCheck)} style={{fontSize: "1.5em"}}></i><b>{lightFilterCheck ?`Light > ${lightFilterValue} % `: 'Light'}</b>
                </div>
                <div className='filterSlider' style={{display:lightFilterCheck ? 'block' : 'none' }}>                
                    <Slider value={lightFilterValue} onChange={(e) => setLightFilterValue(e.value)} min={min_filter_range} max={max_filter_range} />
                </div>
            </div>


            <div className='filterContainer'>
                <div className='filterCheckbox' onClick={switchDarkFilterState}>
                    <i className={getFilterDropdownIcon(darkFilterCheck)} style={{fontSize: "1.5em"}}></i><b>{darkFilterCheck ?`Dark > ${darkFilterValue} % `: 'Dark'}</b>
                </div>
                
                <div className='filterSlider' style={{display:darkFilterCheck ? 'block' : 'none' }}>

                    <Slider value={darkFilterValue} onChange={(e) => setDarkFilterValue(e.value)} min={min_filter_range} max={max_filter_range} />
                </div>
            </div>


            <h3>Apply Filter</h3>
            <div className='buttonContainer'>
                <Button label="Filter files" onClick={handleClickFilterFiles}/>
                <Button label="Move selected files"  onClick={handleClickMoveFiles}/>
            </div>


        </div>
    );
}
         
export default Filter