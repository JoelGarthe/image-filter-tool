import './App.css';
import DataTableSortDemo  from './DataTable.js';
import Sidebar from './Sidebar';
import AlertModal from './modals/AlertModal';
import LoadingModal from './modals/LoadingModal';
import ImageModal from './modals/ImageModal';

// Global Styling
import 'primeicons/primeicons.css';
import 'primereact/resources/themes/vela-blue/theme.css';
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';




function App() {
  return (
      
      <div className="main">
        
        <AlertModal/>
        <LoadingModal/>
        <ImageModal/>

        <div className="navigation">
          <Sidebar/>
        </div>
        <div className="mainContent">

          <DataTableSortDemo/>
          
        </div>



      </div>

  );
}

export default App;
