

import React, { useState, useEffect, useRef} from 'react';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { predictRows} from './model.js'
import CustomEventBus from './CustomEventBus.js';
import { Toast } from 'primereact/toast';
import { Checkbox } from 'primereact/checkbox';
const { ipcRenderer } = window;


/**
 * 
 * Main data table component.
 * 
 */



function createData(name, size, blur_value, light_value, dark_value, loadingState) {

    let split_path = name.split('\\')
    let display_name = split_path[split_path.length -1]


    return {
      "name": name,
      "display_name": display_name,
      "size": size,
      "blur_value": blur_value,
      "light_value": light_value,
      "dark_value": dark_value,
      "loadingState": loadingState,
      "selected":false
    };
}

function createFolderPath(stringArray){
    let result = ""

    stringArray.forEach(e => {
        result = result + e + "\\"
    })

    return result
}

const DataTableSortDemo = () => {
    const [rows, setRows] = useState([]);
    const [sortField, setSortField] = useState(null);
    const [sortOrder, setSortOrder] = useState(null);
    const toast = useRef(null);

    useEffect(() => {
        // Save the current state if something changes
        if(rows.length > 0)
            ipcRenderer.send("SaveState", rows)
    })



    const imageDisplayButton = (rowData) => {
        return (
            <React.Fragment>
                <Button icon="pi pi-image" className="p-button-rounded mr-2" onClick={() => CustomEventBus.send("OpenImageModal", {rows: rows, activeRow: rowData})} />
            </React.Fragment>
        );
    }
    
    const checkDataValue = (val) => {
        return val == -1 ? <span className={'product-badge status-warning'}>undetermined</span>: <b>{val} %</b> 
    }

    const dataDisplayBlur = (rowData) => {
        return checkDataValue(rowData['blur_value'])
    }

    const dataDisplayLight = (rowData) => {
        return checkDataValue(rowData['light_value'])
    }

    const dataDisplayDark = (rowData) => {
        return checkDataValue(rowData['dark_value'])
    }

    const showSuccess = () => {
        toast.current.show({severity:'success', summary: 'Images loaded', life: 1000});
    }


    const receiveFileDialogResults = (results) => {
        let newRows = []

        results.forEach(element => {
            newRows.push(createData(element, 0, -1, -1, -1, false))
        });

        console.log("newrows", newRows)
        
        if(newRows.length > 0){
            setRows(newRows)
            
            
            // TODO Use the toast?
            //showSuccess()
        }
            // Update table visually.
            

    }


    ipcRenderer.removeAllListeners("LoadedFiles")
    ipcRenderer.removeAllListeners("LoadState")
    CustomEventBus.removeAllListeners("PredictImages")
    CustomEventBus.removeAllListeners("FilterFiles")
    CustomEventBus.removeAllListeners("ReturnFromGallery")
    CustomEventBus.removeAllListeners("MoveSelectedFiles")
    CustomEventBus.removeAllListeners("OpenMoveFileModalGetPath")

    CustomEventBus.on("MoveSelectedFiles", (selectedFolder) => {

        let selectedRows = rows.reduce(function(result, row) {
            if (row.selected)
                result.push(row);
            return result;
          }, []);

        ipcRenderer.send("MoveSelectedFiles", {
            selectedRows: selectedRows, 
            selectedFolder: selectedFolder
        })
    })

    ipcRenderer.on('LoadState', (event, loadedData) => {
        console.log("Loading data from state...")
        setRows(loadedData)
    })

    CustomEventBus.on("ReturnFromGallery", (newRows) => {
        newRows= [...newRows]
        console.log("Getting changes from image gallery...")
        setRows(newRows)
    })

    
    CustomEventBus.on("OpenMoveFileModalGetPath", () => {

        let filePath = rows[0]['name'].split('\\')

        filePath.pop()

        CustomEventBus.send("OpenMoveFileModal", createFolderPath(filePath) + "\\seperated_images")

    })

    
    
    CustomEventBus.on("FilterFiles", (filter_values) => {
        console.log(filter_values)


        const nextRowsState = rows.map(row => {

            row.selected = false

            if (filter_values['blur'] > 0 && filter_values['blur'] < row['blur_value']){
                row.selected = true
            }
            else if (filter_values['light'] > 0 && filter_values['light'] < row['light_value']){
                row.selected = true
                    
            }
            else if (filter_values['dark'] > 0 && filter_values['dark'] < row['dark_value']){
                row.selected = true
            }

            return row
        })


        setRows(nextRowsState)

    })

    // Receive filepaths from the frontend process
    ipcRenderer.on("LoadedFiles", (event, files) => {
        receiveFileDialogResults(files)
    })

    CustomEventBus.on("PredictImages", async (selectedQuality) => {

        if(rows.length > 0){

            CustomEventBus.send("OpenLoadFileModal")

            let res = await predictRows(rows, selectedQuality)

            // TODO 
            // A trick to make React reload all visual elements
            setRows(res.map(row => {return row}))

        
        
            CustomEventBus.send("CloseLoadFileModal")
        }
    })

    function selectRow(rowData){

        // Array needs to be remapped and set, otherwise the ui does not update
        // Look into useState in combination with arrays for more information
        const nextRowsState = rows.map(row => {
            if(row == rowData){
                row.selected = !row.selected
                return row   
            }
            return row
        })
        setRows(nextRowsState)
    }

    const getCheckmark = (rowData) => {
        return <Checkbox onChange={(e) => selectRow(rowData)} checked={rowData['selected']}></Checkbox>
    }

    const sortFunction = (e) => {
        console.log(e)

        var sorted = [...rows] 

        setSortOrder(e.sortOrder)
        setSortField(e.sortField)

        // Sort ascending
        if (e.sortField && e.sortOrder === 1) 
            sorted = sorted.sort((a, b) => (a[e.sortField] > b[e.sortField] ? 1 : a[e.sortField] < b[e.sortField] ? -1 : 0));

        // Sort Descending
        if (e.sortField && e.sortOrder === -1) 
            sorted = sorted.sort((a, b) => (a[e.sortField] > b[e.sortField] ? -1 : a[e.sortField] < b[e.sortField] ? 1 : 0));

        setRows(sorted)
        
    }
    

    return (
        <div>
            
            <div className="card">
            <DataTable value={rows} responsiveLayout="scroll" sortField={sortField} sortOrder={sortOrder} onSort={sortFunction} >

                    <Column field="selected" body={getCheckmark} header="" headerStyle={{width: '2.5em'}} sortable></Column>
                    <Column field="display_name"  header="Files" sortable></Column>
                    <Column field="blur_value" body={dataDisplayBlur} header="Blur Value" sortable></Column>
                    <Column field="light_value" body={dataDisplayLight} header="Light Value" sortable></Column>
                    <Column field="dark_value" body={dataDisplayDark} header="Dark Value" sortable></Column>
                    <Column body={imageDisplayButton} exportable={false} style={{ minWidth: '4rem' }}></Column>
                </DataTable>
            </div>
            <Toast ref={toast} position="bottom-center"/>

        </div>
    );
}
         
export default DataTableSortDemo