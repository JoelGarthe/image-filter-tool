
const { app, BrowserWindow, ipcMain} = require('electron')
const remoteMain = require('@electron/remote/main');
remoteMain.initialize();
const path = require("path");
var spawn = require('child_process').spawn;
const { npm_lifecycle_event } = process.env;
var executablePath = "./backend/dist/flask_backend/flask_backend.exe";
const FileHandler = require('./fileHandler.js')
const fs = require('fs-extra');
var ls = null
var win = null


function createWindow () {

  // Create the browser window.
  win = new BrowserWindow({
    title: "Blur Detection V 0.2.1",
    width: 1200,
    height: 800,
    webPreferences: {
      nodeIntegration: true,
      webSecurity: false,
      enableRemoteModule: true,
      preload: __dirname + '/preload.js'
    }
  })
  
  // Create event handlers to handle actions that only electron can implement
  //  (everything in the file system)
  ipcMain.on("LoadFiles", () =>{
    FileHandler.loadFiles(win)
  })

  ipcMain.on("MoveSelectedFiles", (event, {selectedRows, selectedFolder}) =>{
    FileHandler.moveFiles(selectedRows, selectedFolder)
  })

  ipcMain.on("SelectMovePath", (event) =>{
    FileHandler.selectMovePath(win)
  })

  ipcMain.on("SaveState", (event, rows) =>{
    FileHandler.saveCurrentState(rows)
    console.log("Saving current state....")
  })

  // Disable the tool menu 
  win.setMenu(null)

  // If the app ist started in dev mode 
  if (npm_lifecycle_event == "start:dev"){

    //load the index.html from a url if this is started from a dev 
    win.loadURL('http://localhost:3000' );


    // Open the chrome devtools on startup
    devtools = new BrowserWindow()
    win.webContents.setDevToolsWebContents(devtools.webContents)
    win.webContents.openDevTools({ mode: 'detach' })

  }
  else 
  {
    
    // Load from the packaged index.html
    win.loadURL(`file://${path.join(__dirname, '../build/index.html')}`);

   // Create log folder
    folderName = "./log/"
    if(!fs.existsSync(folderName))
        fs.mkdirSync(folderName)

    // Create log files
    timestamp = new Date().getTime() 
    filepathLog = `${folderName}${timestamp}.log`
    filepathLogErr = `${folderName}${timestamp}-ERR.log`

    // Setup log streams
    let logConsoleStream = fs.createWriteStream(filepathLog, {flags: 'a'});
    let logErrorStream = fs.createWriteStream(filepathLogErr, {flags: 'a'});

    // Start the flask backend
    ls = spawn(executablePath);

    // Connect the output of the python script to the output file streams
    ls.stdout.pipe(logConsoleStream)
    ls.stderr.pipe(logErrorStream)

  }

  remoteMain.enable(win.webContents)

}

  // This method will be called when Electron has finished
  // initialization and is ready to create browser windows.
  // Some APIs can only be used after this event occurs.
  app.whenReady().then(() =>{
    
    createWindow()
    win.webContents.once('dom-ready', () => {FileHandler.loadState(win)});
    
  });


// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }

  // Try to kill the backend if this application is closed.
  if(ls != null){
    ls.kill()
  }
})



