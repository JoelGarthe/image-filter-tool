
const { contextBridge, ipcRenderer} = require('electron')

// Setup communication between electron and react
contextBridge.exposeInMainWorld('ipcRenderer', {
    on: (event, functionTest) => ipcRenderer.on(event, (event, files) => {
        functionTest(event, files)
    }),

    send: (event_name, event_arguments) => {
        console.log(event_name)
        console.log(event_arguments)
        ipcRenderer.send(event_name,  event_arguments)
    
    },

    removeAllListeners: (name) => ipcRenderer.removeAllListeners(name)
    
});



contextBridge.exposeInMainWorld('electron', {
    openDialog: (method, config) => ipcRenderer.invoke('dialog', method, config),
    sendNotification: (message) => { ipcRenderer.send('notify', message);}
});