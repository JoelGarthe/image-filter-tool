const { fireEvent } = require('@testing-library/react');
const {dialog} = require('electron')
const fs = require('fs-extra')
var path = require("path");
var dir = './tmp';
var statefile = './savestates/save.bsave'

// Config for the file loader
const dialogConfig = {
    filters: [
      { name: 'Images', extensions: ['jpg', 'png'] }
    ],
    title: 'Select files to load',
    buttonLabel: 'Load',
    properties: ['openFile', 'multiSelections']
  };


async function loadState(win){

  console.log("Sending load state command ")
  let rows = JSON.parse( fs.readFileSync(statefile))

  win.webContents.send('LoadState', rows)
}

// Load files from the file system and send the result to the react process
async function loadFiles(win){

    dialog.showOpenDialog(dialogConfig).then((result) => {
        console.log(result.filePaths)
        win.webContents.send('LoadedFiles', result.filePaths)
      });

}

// Config for the path selection dialog
const pathSelectionConfig = {
    filters: [
      { name: 'Folder'}
    ],
    title: 'Select a target folder',
    buttonLabel: 'Select',
    properties: ['openDirectory', 'createDirectory', 'promptToCreate']
  };

// Select a path to move files to and return it to the react process
async function selectMovePath(win){

    dialog.showOpenDialog(pathSelectionConfig).then((result) => {
        console.log(result.filePaths)

        win.webContents.send('SelectedMovePath', result.filePaths[0])
      });

}


// Move a list of files to a target folder
function moveFiles(fileList, targetFolder){

    // If the list of files to move is empty do nothing
    if (fileList.length <= 0)
        return

    // Create the new directory for the seperated files if nessecary
    if (!fs.existsSync(targetFolder)){
        fs.mkdirSync(targetFolder);
    }

    // Move through the file list and move them 
    fileList.forEach(file => {
        fs.rename(file['name'], targetFolder + "\\" + path.basename(file['name']), function (err) {
            if (err) throw err
            console.log('Files successfully moved.')
        })
    });
    
    return true
}

function saveCurrentState(data){
  folderName = "./savestates/"
  if(!fs.existsSync(folderName))
      fs.mkdirSync(folderName)

  fs.writeFile(statefile, JSON.stringify(data), (err) => {
    if (err) throw err;
    console.log('Current state has been saved...');
  }); 

}


module.exports = {moveFiles, loadFiles, selectMovePath, saveCurrentState, loadState}

