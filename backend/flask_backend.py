from flask import Flask
from flask import request
import ai_api_comp as ai

app = Flask(__name__)

@app.route('/predict', methods=['POST'])
def predict():

    if request.method == 'POST':
        print(" * Received POST request. Processing request...")

        print("* Waiting ...")

        request_json = request.json

        result = ai.predict_image_list(request_json["files"], request_json["quality"])

        return result

@app.route('/loadStatus', methods=['GET'])
def loadStatus():

    if request.method == 'GET':
        print(" * Responding with load status...")

        return [ai.getLoadingStatus()]

app.run(port=5029)
        