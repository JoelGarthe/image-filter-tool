from tensorflow import keras
import PIL
import numpy as np 
import os
import cv2 as cv
import gc

IMG_HEIGHT = 1000
IMG_WIDTH = 1000

current_loading_status = -1

# Create absolute filepaths on execution because relatives dont work
filepath_blur = os.path.join(os.path.dirname(__file__), 'model_blur.h5')
filepath_light = os.path.join(os.path.dirname(__file__), 'model_light.h5')
filepath_dark = os.path.join(os.path.dirname(__file__), 'model_dark.h5')
#filepath_noise = os.path.join(os.path.dirname(__file__), 'model_noise.h5')
#filepath_blur_rough = os.path.join(os.path.dirname(__file__), 'model_blur_rough.h5')

# Load all models
model_blur = keras.models.load_model(filepath_blur)
model_light = keras.models.load_model(filepath_light)
model_dark = keras.models.load_model(filepath_dark)
#model_blur_rough = keras.models.load_model(filepath_blur_rough)
#model_noise = keras.models.load_model(filepath_noise)


def getLoadingStatus():
    global current_loading_status

    return current_loading_status


def nearest_res_count(img, factor=2):

    count_height = int(img.size[0] / IMG_HEIGHT)
    count_width = int(img.size[1] / IMG_WIDTH)

    if count_height < count_width:
        return count_height
    return count_width



def cut_img_into_subimages_high(img):

    """
    Cut an image into as many 1000 x 1000 subimages as possible and return them in an array.

    TODO Maybe consider performance testing the loading and cutting and resizing

    Args:
        img : Input image to cut up.

    """
    
    result = []

    # Convert to a pil image to resize
    img = PIL.Image.fromarray(img)
    
    # Calculate the number of horizontal and vertical subimages
    count = img.size[0]//IMG_WIDTH
    count_height = img.size[1]//IMG_HEIGHT

    # If the image is over 2000 x 2000 px
    if (img.size[0]> IMG_WIDTH * 2) and (img.size[1]> IMG_HEIGHT * 2):

        # Resize it to cleanly fit the calculated number of 1000x1000 px subimages
        img_resized = np.array(img.resize((IMG_WIDTH * count, IMG_HEIGHT * count_height), PIL.Image.ANTIALIAS))
    else:

        # Resize the image to 1000 x 1000 px
        return [np.array(img.resize((IMG_WIDTH, IMG_HEIGHT), PIL.Image.ANTIALIAS))]

    # Cut the image into subimages 
    for x in range(count_height):
        for y in range(count):
            temp_sub = img_resized[IMG_WIDTH * x: IMG_WIDTH * (x + 1), IMG_HEIGHT * y: IMG_HEIGHT * (y + 1)]
            result.append(temp_sub) 

    return result     


def cut_img_into_subimages(img):

    """
    Cuts an image into 4 1000 x 1000 subimages and returns them in an array.

    Args:
        img : Input image to cut up.

    """

    result = []

    # TODO Performance Test this
    img = PIL.Image.fromarray(img)
    count = 2

    # If the image in both dimensions is above 2000 px
    if (img.size[0]> IMG_WIDTH * count) and (img.size[1]> IMG_HEIGHT * count):

        # Resize the image to 2000 x 2000 px
        img_resized = np.array(img.resize((IMG_WIDTH * count, IMG_HEIGHT * count), PIL.Image.ANTIALIAS))
    else:

        # Resize the image to 1000 x 1000 px
        return [np.array(img.resize((IMG_WIDTH, IMG_HEIGHT), PIL.Image.ANTIALIAS))]

    # Cut the image into 4 subimages 
    for x in range(count):
        for y in range(count):
            result.append(img_resized[IMG_WIDTH * x: IMG_WIDTH * (x + 1), IMG_HEIGHT * y: IMG_HEIGHT * (y + 1)])       

    return result


def get_histogram_from_image(image):
    """
    Get the histogram from an image in form of a vector.

    Args:
        image (numpy array): Image to get the histogram from.

    """
    histogram, bin_edges = np.histogram(image, bins=256, range=(0, 1))

    histogram = (histogram-np.min(histogram))/(np.max(histogram)-np.min(histogram))

    return np.array(histogram)



def add_edge_layer_to_img(img):

    """
    Return the rgbe format of an image. 

    Args:
        img : Image to convert to rgbe

    """
    
    return np.array([np.dstack((img, cv.Canny(img, 30, 30))) / 255])



def preprocess_image_light(img):

    """
    Preprocess image for light and dark detection.

    Args:
        img : Image to be preprocessed. 

    """
    
    img = np.array(img) / 255

    base_histogram = get_histogram_from_image(img)

    return np.array(base_histogram)





def remove_alpha_channel(img):
    """
    Removes the alpha channel of an input image.

    Args:
        img : Image to remove the alpha channel from. 

    """
    
    numpy_img = np.array(img)

    # Remove alpha channel from png format images 
    if numpy_img.shape[2] == 4:
         numpy_img = numpy_img[:,:,:3]
    
    return numpy_img



def predict_image_list(image_list, quality):
    """
    Predict all relevant values for a list of files with the specified quality.

    Args:
        image_list : File list to be processed.
        quality : Blur detection quality to be used. 

    """
    
    # List to store all results
    resultList = []
    
    # Current number of processed images.
    counter = 0

    # Current loading status
    global current_loading_status
    current_loading_status = 0


    # Loop through all images.
    for image in image_list:

        counter += 1

        # Load the image
        img = PIL.Image.open(image)

        # Remove the alpha channel if there is one present.
        prepared_img = remove_alpha_channel(img)

        sub_images = []

        # Depending on the specified quality cut the image into subimages.
        if quality == "Medium":
            sub_images = cut_img_into_subimages(prepared_img)
        if quality == "Fine":
            sub_images = cut_img_into_subimages_high(prepared_img)

        blur_percentage = 0

        # Loop through all images. 
        for sub_image in sub_images:

            # Prepare input for the different models
            blur_input = add_edge_layer_to_img(sub_image)

            # Pass the image to the network
            result_blur = model_blur.predict(blur_input)

            # Add up all results.
            blur_percentage += float(result_blur[0][0])

            # Free the tensors to use as little memory as possible.
            del result_blur
            del blur_input

        # Calculate the mean blur result of all subimages.         
        blur_percentage = blur_percentage / len(sub_images)

       
        # Preprocess the image for over and underexposure detection.
        light_input = preprocess_image_light(prepared_img)

        # Free the tensors to use as little memory as possible.
        del prepared_img
        del sub_images
        del img

        # Reshape to fit into the light model
        light_input = np.reshape(light_input, (1,256))
        
        print("Processing ", image)

        # Predict the over and underex0osure 
        result_light = model_light.predict(light_input)
        result_dark = model_dark.predict(light_input)
        #result_noise = model_noise.predict(base_output)

        # Calculate the current loading state to return to the frontend
        current_loading_status = (counter / len(image_list)) * 100

        # Append to the list of results
        resultList.append({
            "file": image, 
            "blur": float(blur_percentage),
            "light": float(result_light[0][0]),
            "dark": float(result_dark[0][0]),
        })


    print(" * Finished processing.")

    # Reset the loading state
    current_loading_status = -1 

    # Again, free as much memory as possible
    del image_list
    gc.collect()


    return resultList